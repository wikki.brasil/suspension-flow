import sys
import numpy as np
import os

def print_pc_table(Sn,Kr,var,filename):

    with open(filename, 'w') as writer:
        writer.write('(\n')
        for i in range(len(Sn)):
            writer.write(f'\t({Sn[i]:.8e}\t({Kr[i]:.8e} 0 0))\n')
        writer.write(');\n')

if __name__ == "__main__":

    t_end=10 #[s]

    N=1000
    
    U0 = 100.

    t = np.linspace(0, t_end, num=N, endpoint=True)

    cos = U0*np.cos(t)

    print_pc_table(t, cos, 'cosine','UxTime-cos.dat')


