IOdictionary fluidDict
(
    IOobject
    (
        "fluidProperties",
        mesh.time().constant(),
        mesh,
        IOobject::MUST_READ_IF_MODIFIED,
        IOobject::NO_WRITE
    )
);

Info << "Reading field p\n" << endl;
volScalarField p
(
    IOobject
    (
        "p",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

Info << "Reading field U\n" << endl;
volVectorField U
(
    IOobject
    (
        "U",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

volTensorField A
(
    IOobject
    (
        "A",
        runTime.timeName(),
        mesh,
        IOobject::MUST_READ,
        IOobject::AUTO_WRITE
    ),
    mesh
);

volTensorField E
(
    IOobject
    (
        "E",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    (0.5*(fvc::grad(U) + T(fvc::grad(U))))
);

volTensorField Tau
(
    IOobject
    (
        "Tau",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedTensor("zero", dimensionSet(0, 2, -2, 0, 0), tensor::zero)
);

dimensionedScalar etas("etas", dimensionSet(0, 2, -1, 0, 0), fluidDict.lookup("etas"));
dimensionedScalar beta("beta", dimensionSet(0, 2, -2, 0, 0), fluidDict.lookup("beta"));

dimensionedScalar alpha0("alpha", dimless, fluidDict.lookup("alpha"));
scalar alpha = alpha0.value();

label AmaxIter = fluidDict.lookupOrDefault("AmaxIter", 1);

#include "createPhi.H"


label pRefCell = 0;
scalar pRefValue = 0.0;
setRefCell(p, pimple.dict(), pRefCell, pRefValue);
mesh.schemesDict().setFluxRequired(p.name());

