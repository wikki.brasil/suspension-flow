void HiGFlow_Jacobi(Foam::tensor& E, Foam::vector& lambda, Foam::tensor& R)
{
    R = tensor::zero;
    vector b = vector::zero;
    vector z = vector::zero;
    
    int NMAX = 500;
    int nrot = 0;
    for (int ip = 0; ip < 3; ip++)
    {
        R(ip,ip) = 1.0;
        b(ip) = E(ip,ip);
        lambda(ip) = b(ip);
    }

    for (int i = 1; i <= NMAX; i++)
    {
        scalar tresh;
        scalar sm = 0.0;
        for (int ip = 0; ip < 3 - 1; ip++)
            for (int iq = ip + 1; iq < 3; iq++)
                sm += mag(E(ip,iq));
        if (sm == 0.0)
            return;
        if (i < 4)
            tresh = 0.2 * sm / (3 * 3);
        else
            tresh = 0.0;
        for (int ip = 0; ip < 3 - 1; ip++)
            for (int iq = ip + 1; iq < 3; iq++)
            {
                scalar g = 100.0 * mag(E(ip,iq));
                if ((i > 4) && (mag(lambda(ip)) + g == mag(lambda(ip))) &&
                    (mag(lambda[iq]) + g == mag(lambda(iq))))
                {
                    E(ip,iq) = 0.0;
                }
                else if (mag(E(ip,iq)) > tresh)
                {
                    scalar t, theta;
                    scalar h = lambda(iq) - lambda(ip);
                    if (mag(h) + g == mag(h))
                    {
                        t = E(ip,iq) / h;
                    }
                    else
                    {
                        theta = 0.5 * h / E(ip,iq);
                        t = 1.0 / (mag(theta) + Foam::pow(1.0 + theta * theta, 0.5));
                        if (theta < 0.0)
                            t = -t;
                    }
                    scalar c = 1.0 / Foam::pow(1.0 + t * t, 0.5);
                    scalar s = t * c;
                    scalar tau = s / (1.0 + c);
                    h = t * E(ip,iq);
                    z(ip) = z(ip) - h;
                    z(iq) = z(iq) + h;
                    lambda(ip) = lambda(ip) - h;
                    lambda(iq) = lambda(iq) + h;
                    E(ip,iq) = 0.0;
                    for (int j = 0; j <= ip - 1; j++)
                    {
                        g = E(j,ip);
                        h = E(j,iq);
                        E(j,ip) = g - s * (h + g * tau);
                        E(j,iq) = h + s * (g - h * tau);
                    }
                    for (int j = ip + 1; j <= iq - 1; j++)
                    {
                        g = E(ip,j);
                        h = E(j,iq);
                        E(ip,j) = g - s * (h + g * tau);
                        E(j,iq) = h + s * (g - h * tau);
                    }
                    for (int j = iq + 1; j < 3; j++)
                    {
                        g = E(ip,j);
                        h = E(iq,j);
                        E(ip,j) = g - s * (h + g * tau);
                        E(iq,j) = h + s * (g - h * tau);
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        g = R(j,ip);
                        h = R(j,iq);
                        R(j,ip) = g - s * (h + g * tau);
                        R(j,iq) = h + s * (g - h * tau);
                    }
                    nrot++;
                }
            }
        for (int ip = 0; ip < 3; ip++)
        {
            b(ip) = b(ip) + z(ip);
            lambda(ip) = b(ip);
            z(ip) = 0.0;
        }
    }
}
